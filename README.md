# Otus homeworks

## Brief

This repository intended for storing and demonstration of programming results  throughout passing of [Otus course about Spring framework](https://otus.ru/lessons/javaspring/?int_source=courses_catalog&int_term=programming)

The training repository is located on [github](https://github.com/OtusTeam/Spring)

## Student info
* Student: Ananev Konstantin
* Group: 2021-02

## Homeworks

1) [Homework 1 - First glance at Spring application(Spring context, xml context, pom, maven, junit)](./homework1)